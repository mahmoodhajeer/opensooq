from model.database import User, db, bcrypt
from flask_jwt_extended import create_access_token, jwt_required
from response.UserResponse import access_response, used_email, used_username, wrong_password
'''
from extensions import db


class User(db.Model):
    __tablename__ = 'user'

    id       = db.Column(db.Integer, primary_key=True)
    name     = db.Column(db.String(250),nullable=False)
    username = db.Column(db.String(250),nullable=False)
    email    = db.Column(db.String(250), unique=True)
    password = db.Column(db.String(255), nullable=False)

    item     = db.relationship("Item", backref=db.backref('user', lazy='dynamic' ))

    def __init__(self, name, username, email, password):
        
        self.name     = name
        self.username = username
        self.email    = email
        self.password = password

'''

def check_email(email) :

    if User.query.filter_by(email = email).first():
        return used_email(email)


def check_username(username):

    if User.query.filter_by(username = username).first():
        return used_username(username)
    

def create(data):

    new_user = User(
                name = data['name'],
                username = data['username'] ,
                email = data['email'],
                password = bcrypt.generate_password_hash(data['password'])
            )
    
    db.session.add(new_user)
    db.session.commit()

    access_token = create_access_token(identity = new_user.id)
    
    return access_response(access_token)


def login(data):
        user = User.query.filter_by(email=data['email']).first()
        
        
        if bcrypt.check_password_hash(user.password, data['password']):
            access_token = create_access_token(identity = user.id)
            return access_response(access_token)
        else:
            return wrong_password()

        return {
                'status': 'fail',
                'message': 'somthing wrong,try again later'
            }
        

