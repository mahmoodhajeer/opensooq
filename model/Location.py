'''
from extensions import db

class Location(db.Model):
    __tablename__ = 'location'

    id        = db.Column(db.Integer, primary_key=True)

    contry_id = db.Column(db.Integer, db.ForeignKey('contry.id', ondelete='CASCADE'), nullable=False)
    contry    = db.relationship('Contry', backref=db.backref('location', lazy='dynamic' ))

    city_id   = db.Column(db.Integer, db.ForeignKey('city.id', ondelete='CASCADE'), nullable=False)
    city      = db.relationship('City', backref=db.backref('location', lazy='dynamic' ))

    item_id   = db.Column(db.Integer, db.ForeignKey('item.id'),nullable=False)


    def __init__(self, contry_id, city_id, item_id):
        self.contry_id = contry_id
        self.city_id   = city_id
        self.item_id   = item_id


'''


