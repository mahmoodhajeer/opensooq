'''
from extensions import db

class Contry(db.Model):
    __tablename__ = 'contry'

    id       = db.Column(db.Integer, primary_key=True)
    name     = db.Column(db.String(250),nullable=False)

    city     = db.relationship("City", backref=db.backref('contry', lazy='dynamic' ))
    location = db.relationship("Location", backref=db.backref('contry', lazy='dynamic' ))



    def __init__(self, name):
        self.name = name

'''