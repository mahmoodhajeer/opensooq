from model.database import Category, City, Contry, Image, Item, bcrypt, db
from response.ItemResponse import added_item, get_all_items
import os
import datetime
from werkzeug.utils import secure_filename
from config import UPLOAD_FOLDER
from flask_jwt_extended import get_jwt_identity, jwt_required

'''
from extensions import db

item_category = db.Table('item_category',
                db.Column('item_id', db.Integer, db.ForeignKey('item.id')),
                db.Column('category_id', db.Integer, db.ForeignKey('category.id'))
                )


class Item(db.Model):
    __tablename__ = 'item'

    id           = db.Column(db.Integer, primary_key=True)
    Name         = db.Column(db.String(250),nullable=False)
    description  = db.Column(db.String(250),nullable=False)

    user_id      = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    user         = db.relationship('User', backref=db.backref('item', lazy='dynamic' ))

    location     = db.relationship('Location', backref=db.backref('item', lazy='dynamic' ))
    category     = db.relationship("Category",secondary=item_category)


    def __init__(self, Name, description, user_id):
        self.Name        = Name
        self.description = description
        self.user_id     = user_id

'''

def newitem(data,file):
    

    new_item = Item(
                name = data['name'],
                description = data['description'] ,
                user_id = get_jwt_identity(),
                city_id = data['city_id']
            )


    db.session.add(new_item)


    for category_id in data['category']:
        category = Category.query.filter_by(id = category_id).first()
        category.categoryitem.append(new_item)

    db.session.commit()
    filename = secure_filename(file.filename)
    file.save(os.path.join(UPLOAD_FOLDER, filename))

    new_image = Image(
    path = filename,
    item_id = new_item.id
    )
    db.session.add(new_image)
    db.session.commit()

    
    return added_item()


def get_items():
    items = Item.query.join(City, City.id == Item.city_id).join(Contry, Contry.id == City.contry_id).join(Category, Item.category).all()
    
    
    '''from response.ItemResponse import ItemSchema
    result = ItemSchema(many=True).dump(items).data'''

    return get_all_items(items)
    

        