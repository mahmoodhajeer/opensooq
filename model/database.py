from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from dataclasses import field



db = SQLAlchemy()
bcrypt = Bcrypt()

class User(db.Model):
    __tablename__ = 'user'

    id       = db.Column(db.Integer, primary_key=True)
    name     = db.Column(db.String(250),nullable=False)
    username = db.Column(db.String(250),nullable=False)
    email    = db.Column(db.String(250), unique=True)
    password = db.Column(db.String(255), nullable=False)

    item     = db.relationship('Item',backref=db.backref('user'))




item_category = db.Table('item_category',
                db.Column('item_id', db.Integer, db.ForeignKey('item.id')),
                db.Column('category_id', db.Integer, db.ForeignKey('category.id')), 
                db.PrimaryKeyConstraint('item_id', 'category_id')
                )


class Item(db.Model):
    __tablename__ = 'item'

    id           = db.Column(db.Integer, primary_key=True)
    name         = db.Column(db.String(250),nullable=False)
    description  = db.Column(db.String(250),nullable=False)
    user_id      = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    city_id      = db.Column(db.Integer, db.ForeignKey('city.id', ondelete='CASCADE'))
    
    category     = db.relationship("Category",secondary=item_category, backref=db.backref('categoryitem', lazy='dynamic'))
    image        = db.relationship('Image',backref=db.backref('imageitem'))

        

class users(db.Model):
    __tablename__ = 'users'

    id           = db.Column(db.Integer, primary_key=True)
    name         = db.Column(db.String(250),nullable=False)
    city         = db.Column(db.String(250),nullable=False)
    location     = db.Column(db.String(250),nullable=False)
    number       = db.Column(db.String(250),nullable=False)
    

class Contry(db.Model):
    __tablename__ = 'contry'

    id       = db.Column(db.Integer, primary_key=True)
    name     = db.Column(db.String(250),nullable=False)

    contry    = db.relationship("City", backref=db.backref('contry'))




class City(db.Model):
    __tablename__ = 'city'

    id        = db.Column(db.Integer, primary_key=True)
    name      = db.Column(db.String(250),nullable=False)

    contry_id = db.Column(db.Integer, db.ForeignKey('contry.id'), nullable=False)

    city      = db.relationship('Item',backref=db.backref('city'))    
    
class Category(db.Model):
    __tablename__ = 'category'

    id      = db.Column(db.Integer, primary_key=True)
    name    = db.Column(db.String(250),nullable=False)

class Image(db.Model):
    __tablename__ = 'image'

    id      = db.Column(db.Integer, primary_key=True)
    path    = db.Column(db.String(250),nullable=False)
    item_id = db.Column(db.Integer, db.ForeignKey('item.id', ondelete='CASCADE'), nullable=False)

