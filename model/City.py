'''
from extensions import db

class City(db.Model):
    __tablename__ = 'city'

    id        = db.Column(db.Integer, primary_key=True)
    name      = db.Column(db.String(250),nullable=False)

    contry_id = db.Column(db.Integer, db.ForeignKey('contry.id', ondelete='CASCADE'), nullable=False)
    Contry    = db.relationship('Contry', backref=db.backref('city', lazy='dynamic' ))

    location  = db.relationship("Location", backref=db.backref('city', lazy='dynamic' ))
    


    def __init__(self, name, contry_id):
        self.name      = name
        self.contry_id = contry_id
        
'''