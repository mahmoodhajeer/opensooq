
'''
from extensions import db


class Category(db.Model):
    __tablename__ = 'category'

    id      = db.Column(db.Integer, primary_key=True)
    name    = db.Column(db.String(250),nullable=False)

    
    def __init__(self, name):
        self.name = name
        
'''