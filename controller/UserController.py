from flask import jsonify, request, Blueprint
from flask_restful import Api, Resource
from model import User
from flask_restful_swagger import swagger




class UserResource(Resource):
    @swagger.model
    @swagger.operation(notes="some hawa")
    def post(self):
        data = request.get_json(force=True)
        
        if not data:
               return {'message': 'No input data provided'}, 400

        check_email = User.check_email(data['email'])
        check_username = User.check_username(data['username'])

        if check_email :
            return check_email
        elif check_username :
            return check_username
        else:
            return User.create(data)



class LoginResource(Resource):
    @swagger.model
    @swagger.operation(notes="some hawa")
    def post(self,username):
        
        data = request.get_json(force=True)
        if not data:
            return {'message': 'No input data provided'}, 400
        
        check_email = User.check_email(data['email'])
        if check_email :
            return User.login(data)
        else:
            return {
            'status': 'fail',
            'message': 'email {} doesn\'t exist'.format(data['email'])
            }