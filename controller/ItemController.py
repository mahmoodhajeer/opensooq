from flask import Flask, jsonify, request, Blueprint
from flask_restful import Resource
from model import Item
import os
import datetime
from werkzeug.utils import secure_filename
from config import UPLOAD_FOLDER
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful_swagger import swagger
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

app = Flask(__name__)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["200 per day", "50 per hour"]
)





class NewItemResource(Resource):
    @jwt_required
    def post(self):
        
        if 'file' not in request.files:
            return {'message': 'No file part in the request'}, 400

        file = request.files['file']
        if file.filename == '' :
            return {'message': 'No file selected for uploading'}, 400

        data = request.values
        if not data:
               return {'message': 'No input data provided'}, 400
        else:
            return Item.newitem(data,file)

class GitItemsResource(Resource):
    decorators = [limiter.limit("2/day")]
    @swagger.model
    @swagger.operation(notes="some hawa")
    def get(self):
        return Item.get_items()




def allowed_file(filename):
    allowed_extenstion = set(['txt','pdf','png','jpg','jpeg','gif'])
    return '.' in filename and filename.split('.',1)[1].lower() in allowed_extenstion


        



