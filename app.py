from flask import Flask, send_from_directory
from flask_jwt_extended import JWTManager
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_jwt_extended import get_jwt_identity, jwt_required
from flasgger import Swagger
from flasgger.utils import swag_from
from flask_restful_swagger import swagger
from flask_restful import Api




def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)
    


    from model.database import db
    db.init_app(app)

    api = Api(app)

    

    itemapi = swagger.docs(Api(app), apiVersion='1', api_spec_url="/docs")
    userapi = swagger.docs(Api(app), apiVersion='1', api_spec_url="/doc")

    
    from controller.ItemController import GitItemsResource
    itemapi.add_resource(GitItemsResource, '/item/getitems')

    from controller.UserController import UserResource, LoginResource
    userapi.add_resource(UserResource, '/user/registration')
    userapi.add_resource(LoginResource, '/user/login')

    from controller.ScrapController import ScrapingResource, SaveCsvResource
    userapi.add_resource(ScrapingResource, '/scraping/opensooq')
    userapi.add_resource(SaveCsvResource, '/savefile/SaveAsCsv')

    return app




app = create_app("config")
jwt = JWTManager(app)
    
    



