import requests
import urllib.request
import time 
from bs4 import BeautifulSoup
import json 
import csv 

file = open('sooq.json','w',encoding = 'utf8')

file.write('{links :[')
data = {}
for page in range(1523):
    print('----',page,'-----')
    url = 'https://jo.opensooq.com/ar/find?have_images=&allposts=&onlyPremiumAds=&onlyDonation=&onlyPrice=&onlyUrgent=&onlyShops=&onlyMemberships=&onlyBuynow=&memberId=&sort=record_posted_date.desc&term=&cat_id=&scid=60&city=%D8%A5%D8%B1%D8%A8%D8%AF&page={}&per-page=30'.format(str(page)) 
    print(url)

    r = requests.get(url) 
    soup = BeautifulSoup(r.content,"html.parser")
    ancher = soup.find_all('li',{'class':'rectLi ie relative mb15'})
    for pt in ancher:
        link = pt.find('a',{'class':'overflowHidden rectLiImg tableCell vMiddle p8'})
        if link: 
           print(link.get("href"))
           json_data = json.dumps(link.get("href"),ensure_ascii=False)
           file.write(json_data)
           file.write(',')
file.write('] }')
file.close()