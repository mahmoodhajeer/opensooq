from marshmallow import Schema, fields
from response.UserResponse import UserSchema
from response.CategoryResponse import CategorySchema
from response.CityResponse import CitySchema
from response.ContryResponse import ContrySchema
from flask_marshmallow import Marshmallow
from config import UPLOAD_FOLDER




def added_item():
    response_object = {
        'status': 'success'
    }
    return response_object, 201

def get_all_items(items):
    data=[]
    for item in items:
        recordObject = {'id': item.id,
                    'name': item.name,
                    'description': item.description,
                    'user': {
                        'id':item.user.id,
                        'name':item.user.name,
                        'username':item.user.username,
                        'email':item.user.email
                        },
                    'city':   {
                        'id' :item.city.id,
                        'name':item.city.name
                        },
                    'contry':   {
                        'id' :item.city.contry.id,
                        'name':item.city.contry.name
                        },
                    'category':[]
                    
                }
        for categorys in item.category:
            category = {
                'id': categorys.id,
                'name':  categorys.name
                     
                }
            recordObject['category'].append(category)

        data.append(recordObject)
        
    return {'status': 'success', 'data': data}, 200

class ItemSchema(Schema):
    user = fields.Nested(UserSchema)
    city = fields.Nested(CitySchema)
    category = fields.List(fields.Nested(CategorySchema))
    id = fields.Integer()
    name = fields.String()
    description = fields.String()
    

