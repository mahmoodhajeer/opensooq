from marshmallow import Schema, fields
from flask_marshmallow import Marshmallow

from response.ContryResponse import ContrySchema


class CitySchema(Schema):

    id = fields.Integer()
    name = fields.String()
    contry = fields.Nested(ContrySchema)

    