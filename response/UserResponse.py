from marshmallow import Schema, fields
from flask_marshmallow import Marshmallow

def used_email(email):
    response_object = {
            'status': 'fail',
            'message': 'email {} is exist Please Log in'.format(email)
        }
    return response_object, 409

def used_username(username):
    response_object = {
            'status': 'fail',
            'message': 'username {} is exist Please Log in'.format(username)
        }
    return response_object, 409

def access_response(access_token):
    response_object = {
        'status': 'success',
        'access_token': access_token
    }
    return response_object, 201

def wrong_password():
    response_object = {
            'status': 'fail',
            'message': 'wrong password'
        }
    return response_object, 409


class UserSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    username = fields.String()
    email = fields.String()
    