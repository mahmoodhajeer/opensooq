from marshmallow import Schema, fields
from flask_marshmallow import Marshmallow


class ContrySchema(Schema):
    id = fields.Integer()
    name = fields.String()
    